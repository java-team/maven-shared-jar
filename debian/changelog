maven-shared-jar (3.1.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - New dependency on libplexus-xml-java
    - New dependency on libmodello-maven-plugin-java
    - Updated the Maven rules

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 24 Oct 2024 16:07:56 +0200

maven-shared-jar (3.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Build depend on libeclipse-sisu-maven-plugin-java
      instead of libplexus-component-metadata-java
    - Depend on libcommons-collections4-java
      instead of libcommons-collections3-java
    - New build dependency on libplexus-testing-java
    - Build depend on junit5 instead of junit
  * Removed the -java-doc package
  * Standards-Version updated to 4.7.0
  * Switch to debhelper level 13
  * Use salsa.debian.org Vcs-* URLs
  * Track and download the new releases from GitHub

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 04 Aug 2024 12:48:44 +0200

maven-shared-jar (1.2-3) unstable; urgency=medium

  * Team upload.
  * Depend on libplexus-container-default1.5-java instead of
    libplexus-containers-java
  * Standards-Version updated to 4.1.0

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 30 Aug 2017 12:53:24 +0200

maven-shared-jar (1.2-2) unstable; urgency=medium

  * Team upload.
  * Depend on libmaven3-core-java instead of libmaven2-core-java
  * Standards-Version updated to 4.0.0
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 31 Jul 2017 00:52:16 +0200

maven-shared-jar (1.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - New dependency on libmaven-shared-utils-java
    - Removed 0001-add-missing-dependency.patch
    - Updated the Maven rule for bcel
    - Replaced the build dependency on libplexus-maven-plugin-java
      with libplexus-component-metadata-java
    - New build dependency on junit
  * Build with the DH sequencer instead of CDBS
  * debian/control:
    - Standards-Version updated to 3.9.7 (no changes)
    - Use secure URLs for the Vcs-* fields
  * debian/orig-tar.sh:
    - Use XZ compression for the upstream tarball
    - Do not remove the jar files used by the tests
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 27 Feb 2016 01:12:55 +0100

maven-shared-jar (1.1-1) unstable; urgency=low

  * Initial release (Closes: #598433)

 -- Torsten Werner <twerner@debian.org>  Thu, 15 Sep 2011 19:59:25 +0200
